hKiosk Set Hostname Service
===========================

[![MIT license][license-image]][license-link]

A simple systemd service that sets the hostname of the system to the value defined by `SET_HOSTNAME` variable in the
`/media/.set/hostname` file - that is automatically removed once the hostname is updated. Service continuously monitors
the file for changes and updates the hostname accordingly.

The hostname is updated using the `hostnamectl` command.

Installation
------------

```shell
DESTDIR= make install
```

Environment Variables
---------------------

* **`SET_HOSTNAME`**
  * The hostname to be set.
* **`LOOPBACKIP`**
  * The loopback IP address used to set the hostname in `/etc/hosts`.
  * Default: `127.0.1.1`

Usage
-----

Create a `/media/.set/hostname` file containing the variable `SET_HOSTNAME` set to the desired hostname.

```dotenv
SET_HOSTNAME="my-new-hostname"
```

License
-------

[MIT][license-link]


[license-image]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[license-link]: LICENSE
