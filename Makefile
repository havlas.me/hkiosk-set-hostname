.PHONY: install
install:
	install -d $(DESTDIR)/usr/lib/systemd/system
	install -m 644 systemd/hkiosk-set-hostname.service $(DESTDIR)/usr/lib/systemd/system/
	install -m 644 systemd/hkiosk-set-hostname.path $(DESTDIR)/usr/lib/systemd/system/
	install -d $(DESTDIR)/usr/libexec
	install -m 755 libexec/hkiosk-set-hostname $(DESTDIR)/usr/libexec/

.PHONY: uninstall
uninstall:
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-hostname.service
	-rm $(DESTDIR)/usr/lib/systemd/system/hkiosk-set-hostname.path
	-rm $(DESTDIR)/usr/libexec/hkiosk-set-hostname
